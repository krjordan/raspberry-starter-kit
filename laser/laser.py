#!/usr/bin/env python
import RPi.GPIO as GPIO
import time

LedPin = 11

def setup():
    GPIO.setmode(GPIO.BOARD) # Numbers GPIOs by physical location
    GPIO.setup(LedPin, GPIO.OUT) # Set LedPin's mode is output
    GPIO.output(LedPin, GPIO.HIGH) # Set LedPin high(+3.3V) to off led

def loop():
    while True:
        print '...Laser on'
        GPIO.output(LedPin, GPIO.LOW)
        time.sleep(0.5)
        print 'Laser off...'
        GPIO.output(LedPin, GPIO.HIGH)
        time.sleep(0.5)

def destroy():
    GPIO.output(LedPin, GPIO.HIGH)
    GPIO.cleanup()

if __name__ == '__main__':
    setup()
    try:
        loop()
    except KeyboardInterrupt:
        destroy()
