#!/usr/bin/env python
import RPi.GPIO as GPIO
import time

LEDPin = 11 #pin 11
GPIO.setmode(GPIO.BOARD) # Numbers GPIOs by physical location
GPIO.setup(LEDPin, GPIO.OUT) # Set LEDPin's mode as output
GPIO.output(LEDPin, GPIO.HIGH) # Set LEDPin as high(+3.3V) to turn of the led

try:
    while True:
        print '...led on'
        GPIO.output(LEDPin, GPIO.LOW) # led on
        time.sleep(0.5)
        print 'led off...'
        GPIO.output(LEDPin, GPIO.HIGH) # led off
        time.sleep(0.5)
except KeyboardInterrupt: # When 'Ctrl+C' is pressed , the following code will be executed.
    GPIO.output(LEDPin, GPIO.HIGH) # led off
    GPIO.cleanup() # Release resources
